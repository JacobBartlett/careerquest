//
//  ScanViewController.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 07/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import AVFoundation
import UIKit

class ScanViewController: UIViewController {
    
    @IBOutlet var bottomBar: UIView!
    @IBOutlet var qrDownloadButton: UIButton!
    
    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    var qrCodeLink: URL?
    var activeScan: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initiateQRSearch()
    }

    private func initiateQRSearch() {
        if let captureDevice = discoverCaptureDevice() {
            do {
                //Initialize capture session
                captureSession = AVCaptureSession()
                
                // Get an instance of the AVCaptureDeviceInput class using the previous device object.
                let input = try AVCaptureDeviceInput(device: captureDevice)
                
                initiateCaptureSession(captureDevice, input)
                
                configureBottomBar()
                configureQRFrame()
                
            } catch {
                // If any error occurs, simply print it out and don't continue any more.
                print(error)
                return
            }
        }
        else {
            noCaptureDeviceError()
        }
    }
    
    private func discoverCaptureDevice() -> AVCaptureDevice? {
        // Get the back-facing camera for capturing videos
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back)
        
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("Failed to get the camera device")
            return nil
        }
        
        return captureDevice
    }
    
    private func initiateCaptureSession(_ captureDevice: AVCaptureDevice, _ input: AVCaptureDeviceInput) {
        captureSession?.addInput(input)
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        captureSession?.startRunning()
    }

    private func configureBottomBar() {
        bottomBar.backgroundColor = UIColor(displayP3Red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 0.8)
        view.bringSubview(toFront: bottomBar)
        setButtonToDefault()
    }
    
    private func configureQRFrame() {
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubview(toFront: qrCodeFrameView)
        }
    }
    
    private func noCaptureDeviceError() {
        //TODO error message/alert popup explaining that no device was found
    }
    
//    private func checkQRImage() {
//        if let url = qrCodeLink {
//            let networking = NetworkingManager.shared
//            networking.downloadImage(url) { data in
//                if let data = data {
//                    CoreDataService.shared.saveBadge(data, url)
////                    self.badgeFoundAlert(url, data)
//                }
//            }
//        }
//    }
    
    @IBAction func qrDownloadButtonPressed(_ sender: Any) {
//        if let url = qrCodeLink {
//            let networking = NetworkingManager.shared
//            networking.downloadImage(url) { data in
//                if let data = data {
//                    CoreDataService.shared.saveBadge(data, url)
//                }
//            }
//        }
        Variables.fraudulentBadge = true
        badgeFoundAlert()
    }
    
    private func badgeFoundAlert() { //_ url: URL, _ data: Data) {
        let ac = UIAlertController(title: "Badge found", message: "'Attended Mashhup Day' badge added to your profile!", preferredStyle: .alert)

//        let imageFromData = UIImage(data: data)
//        let imageViewContainer = UIImageView(frame: CGRect(x: 0, y: 00, width: 120, height: 120))
//        imageViewContainer.image = imageFromData
//        ac.view.addSubview(imageViewContainer)
        
//        let viewAction = UIAlertAction(title: "Save to profile", style: .default, handler: { Void in
//            CoreDataService.shared.saveBadge(data, url)
////                self.goToProfileTab()
//        })
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//        ac.addAction(viewAction)
        ac.addAction(okAction)
        present(ac, animated: true, completion: nil)
    }
    
}

extension ScanViewController: AVCaptureMetadataOutputObjectsDelegate {

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
//            setButtonToDefault()
            return
        }
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        if activeScan && metadataObj.type == AVMetadataObject.ObjectType.qr {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            if let link = metadataObj.stringValue {
                qrCodeLink = URL(string: link)!
//                activeScan = false
                activateQRButton() //link)
//                checkQRImage()
            }
        }
    }
    
    private func setButtonToDefault() {
        qrCodeFrameView?.frame = CGRect.zero
        qrDownloadButton.setTitle("No QR code detected", for: .normal)
        qrDownloadButton.titleLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: .regular)
        qrDownloadButton.isEnabled = false
        bottomBar.backgroundColor = UIColor(displayP3Red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 0.8)
    }

    private func activateQRButton() { //_ link: String) {
        qrDownloadButton.setTitle("Download Badge", for: .normal)
        qrDownloadButton.titleLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
        qrDownloadButton.isEnabled = true
        bottomBar.backgroundColor = UIColor.darkBlueFaded() //(displayP3Red: 10.0/255.0, green: 10.0/255.0, blue: 10.0/255.0, alpha: 0.9)
    }
    
}
