//
//  Variables.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 08/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

struct Variables {
    static var fraudulentBadge = false
    static var username = ""
    static var grade = "A"
}
