//
//  NetworkingManager.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 07/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation

class NetworkingManager {
    
    static let shared = NetworkingManager()
    
//    public func downloadJSON(_ url: URL, _ completion: @escaping ([AnyObject?]) -> ()) {
//        let urlSession = createUrlSession()
//        urlSession.dataTask(with: url) { data, response, error in
//            if error != nil {
//                completion(nil)
//            }
//            if let data = data {
//                do {
//                    let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
//                    completion()
////                    to call this
////                    write downloadJSON(url) { (data) in
////                        *do stuff to data you get*
////                    }
////
//                    
//                }
//                catch {
//                    print("Error parsing JSON")
//                    completion(nil)
//                }
//            }
//            else {
//                completion(nil)
//            }
//        }.resume()
//    }

    public func downloadImage(_ url: URL, completion: @escaping (Data?) -> ()) {
        let urlSession = createUrlSession()
        urlSession.dataTask(with: url) { data, response, error in
            if error != nil {
                completion(nil)
            }
            if let data = data {
                completion(data)
            }
            else {
                completion(nil)
            }
        }.resume()
    }
    
    private func createUrlSession() -> URLSession {
        let sessionConfig = URLSessionConfiguration.default
        let urlSession = URLSession(configuration: sessionConfig)
        return urlSession
    }

}
