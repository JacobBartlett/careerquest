//
//  PersistenceManager.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 07/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import CoreData

class CoreDataService {
    
    static let shared = CoreDataService()
    
    public func saveQuest() {
        let context = DataManager.shared.mainContext
        let entity = NSEntityDescription.entity(forEntityName: "Quest", in: context)!
        let object = NSManagedObject(entity: entity, insertInto: context)
//        setQuest(object, data, url)
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }


    public func getAllQuests() -> [Quest] {
        let context = DataManager.shared.mainContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Quest")
        do {
            let fetchedEntities = try context.fetch(fetchRequest)
            return (fetchedEntities as! [Quest])
        } catch {
            print("Could not fetch from Core Data: \(error)")
            return []
        }
    }
    
//    private func setQuest(_ object: NSManagedObject) {
//        object.setValue(data, forKey: "data")
//        object.setValue(url.path, forKey: "url")
//    }
    
}
