//
//  Quest.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 08/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation

class Quest {
    
    var username: String
    var grade: String
    var category: String?
    var subCategory: String?
    var title: String
//    var dateStarted: Date
//    var dateCompleted: Date?
    
    init(username: String, grade: String, title: String) { //category: String, subCategory: String, title: String) {
        self.username = username
        self.grade = grade
//        self.category = category
//        self.subCategory = subCategory
        self.title = title
//        self.dateStarted = Date()
//        self.dateCompleted = nil
    }
    
}
