//
//  UIColor+Extensions.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 08/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static func pastelGreen() -> UIColor {
        return UIColor(displayP3Red: 232.0/255.0, green: 245.0/255.0, blue: 233.0/255.0, alpha: 1.0)
    }
    
    static func pastelBlue() -> UIColor {
        return UIColor(displayP3Red: 227.0/255.0, green: 242.0/255.0, blue: 253.0/255.0, alpha: 1.0)
    }
    
    static func darkBlueFaded() -> UIColor {
        return UIColor(displayP3Red: 0/255.0, green: 0/255.0, blue: 205.0/255.0, alpha: 0.8)
    }
    
    static func pastelRed() -> UIColor {
        return UIColor(displayP3Red: 252.0/255.0, green: 228.0/255.0, blue: 236.0/255.0, alpha: 1.0)
    }
    
}
