//
//  UIView+Extensions.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 08/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func cutCorners() {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 5
    }
    
}
