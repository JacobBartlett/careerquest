//
//  QuestsViewController.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 07/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

class QuestsViewController: UIViewController {
    
    @IBOutlet var newQuestButton: UIButton!
    @IBOutlet var questsTableView: UITableView!
    
    var quests: [(String, String, String, Int)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newQuestButton.cutCorners()
        
        quests = [("Course", "UX", "Completed UX 202", 2), ("Certifications", "Business Analyst", "Scrum Master Expert Certification", 1), ("Languages", "Java", "Read Clean Code", 1), ("Languages", "JavaScript", "Created my personal website", 2), ("Languages", "Python", "Took Google Machine Learning Course", 2), ("Languages", "JavaScript", "Learned Advanced Node on LinkedIn Learning", 3), ("Languages", "Java", "Completed Advanced OOP on Pluralsight", 1), ("Platforms", "iOS", "Published my own app onto the App Store", 1), ("Platforms", "AWS", "Completed AWS certification", 3), ("Course", "Android", "Completed mobile development course on Pluralsight", 2), ("Languages", "Java", "Taught Java to new joiner while managing them", 1)]
        questsTableView.dataSource = self
        questsTableView.delegate = self
    }

}

extension QuestsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quests.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "questCell") as! QuestTableViewCell
        cell.category.text = quests[indexPath.row].0
        cell.subCategory.text = quests[indexPath.row].1
        cell.quest.text = quests[indexPath.row].2
        switch quests[indexPath.row].3 {
        case 1:
            cell.backgroundColor = UIColor.pastelBlue()
        case 2:
            cell.backgroundColor = UIColor.pastelGreen()
        case 3:
            cell.backgroundColor = UIColor.pastelRed()
        default:
            cell.backgroundColor = UIColor.white
        }
        return cell
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let questVC = UIStoryboard(name: "Quests", bundle: nil).instantiateViewController(withIdentifier: "QuestDetailViewController") as! QuestDetailViewController
//        questVC.quest = quests[indexPath.row].2
        questVC.quest = Quest(username: "jbartlett", grade: "A", title: quests[indexPath.row].2)
        questVC.quest.category = "Courses"
        questVC.quest.subCategory = "Internal"
        navigationController?.pushViewController(questVC, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showQuestSegue" {
            if let questDetailVC = segue.destination as? QuestDetailViewController {
//                questDetailVC.quest = quests[indexPath.row]
            }
        }
    }
}
