//
//  NewQuestViewController.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 07/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

class NewQuestViewController: UIViewController {
    
    @IBOutlet var titleButton: UIButton!
    @IBOutlet var categoryButton: UIButton!
    @IBOutlet var subCategoryButton: UIButton!
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var beginQuestButton: UIButton!
    @IBOutlet var selectQuestLabel: UILabel!
    
    var prePopulated: Bool = false
    
//    var questTree: [AnyObject] = []
    var categoryList: [String] = ["Certifications", "Courses", "Languages", "Platforms"]
    var selectedCategory: String = ""
    
//    var questTreeSub:  [String: AnyObject] = [:]
    var subCategoryList: [String] = ["Java", "JavaScript", "Python", "Android", "iOS", "SalesForce", "Hybris", "Adobe"]
    var selectedSubCategory: String = ""
    
    var titleList: [String] = ["Read Clean Code", "Tutored new joiner", "Completed Advanced OOP online", "Created personal website", "Tutored new joiner", "Created first system in Node.js", "Completed Google Machine Learning Basics couse", "Completed Salesforce 1 certification", "Read Cormen's Statistical Analytics"]
    var selectedTitle: String = ""
    
    var activeButton: ActiveButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonWrapping()
        beginQuestButton.isEnabled = false
        if !prePopulated {
            setupButtons()
        }
        else {
            buttonTitlePrepop()
        }
//        setupQuestTree()
        pickerView.isHidden = true
        pickerView.dataSource = self
        pickerView.delegate = self
    }
    
    private func buttonWrapping() {
        categoryButton.titleLabel?.lineBreakMode = .byWordWrapping
        categoryButton.titleLabel?.lineBreakMode = .byWordWrapping
        titleButton.titleLabel?.lineBreakMode = .byWordWrapping
    }
    
    private func setupButtons() {
        categoryButton.isEnabled = true
        subCategoryButton.isEnabled = false
        titleButton.isEnabled = false
        
        beginQuestButton.cutCorners()
    }
    
    private func buttonTitlePrepop() {
        categoryButton.setTitle(selectedCategory, for: .normal)
        subCategoryButton.setTitle(selectedSubCategory, for: .normal)
        titleButton.setTitle(selectedTitle, for: .normal)
    }
    
//    private func setupQuestTree() {
//        guard let fileUrl = Bundle.main.url(forResource: "QuestTree", withExtension: "json") else { return }
//        do {
//            let json = try Data(contentsOf: fileUrl)
//            let object = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
//            let tree = (object as! [String: AnyObject])["Tree"]
//            questTree = tree as! [AnyObject]
//            parseKeysFromTree()
//        }
//        catch {
//            print("Failed to parse JSON")
//        }
//    }
    
    @IBAction func categoryButtonPressed(_ sender: Any) {
        activeButton = .category
        buttonPressed()
    }
    
    @IBAction func subCategoryButtonPressed(_ sender: Any) {
        activeButton = .subCategory
        buttonPressed()
    }
    
    @IBAction func titleButtonPressed(_ sender: Any) {
        activeButton = .title
        buttonPressed()
    }
    
    enum ActiveButton {
        case category
        case subCategory
        case title
    }
    
    private func buttonPressed() {
        resetButtonsToDefault()
        pickerView.isHidden = false
        pickerView.reloadAllComponents()
    }
    
    private func resetButtonsToDefault() {
        if activeButton == .title {
            return
        }
        titleButton.setTitle("Quest Name", for: .normal)
        
        if activeButton == .subCategory {
            titleButton.isEnabled = false
        }
        
        if activeButton == .category {
            subCategoryButton.setTitle("SubCategory", for: .normal)
            subCategoryButton.isEnabled = false
            titleButton.isEnabled = false
        }
    }
    
    @IBAction func beginQuestButtonPressed(_ sender: Any) {
//        if Variables.username != "" && selectedTitle != "" {
        if selectedTitle != "" {
//            let newQuest = Quest(username: Variables.username, grade: Variables.grade, category: selectedCategory, subCategory: selectedSubCategory, title: selectedTitle)
//            pushQuestToFirebase()
            navigationController?.popViewController(animated: true)
        }
    }
//
//    @IBAction func beginQuestPressed(_ sender: Any) {
//        navigationController?.popViewController(animated: true)
//    }
    
//    private func pushQuestToFirebase() {
//        // TODO push quest to firebase
//    }
    
    //    private func parseKeysFromTree() {
//        switch activeButton {
//        case .category?:
//            return
//
//        case .subCategory?:
//            return
//
//        default:
//            for quest in questTree {
//                var newQuest = quest as! Array<String, AnyObject>
//                print(newQuest)
//                print(newQuest.key)
//            }
//
//        }

    
    
}

extension NewQuestViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch activeButton {
        case .category?:
            return categoryList.count
            
        case .subCategory?:
            return subCategoryList.count
            
        case .title?:
            return titleList.count
            
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch activeButton {
        case .category?:
            return categoryList[row]
        
        case .subCategory?:
            return subCategoryList[row]

        case .title?:
            return titleList[row]

        default:
            return "Error"

        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let row = pickerView.selectedRow(inComponent: component)

        switch activeButton {
        case .category?:
            selectedCategory = categoryList[row]
            categoryButton.setTitle(selectedCategory, for: .normal)
            subCategoryButton.isEnabled = true

        case .subCategory?:
            selectedSubCategory = subCategoryList[row]
            subCategoryButton.setTitle(selectedSubCategory, for: .normal)
            titleButton.isEnabled = true

        case .title?:
            selectedTitle = titleList[row]
            titleButton.setTitle(selectedTitle, for: .normal)
            beginQuestButton.isEnabled = true
            selectQuestLabel.isHidden = true

        default:
            return

        }
        
        pickerView.isHidden = true
    }
    
}
