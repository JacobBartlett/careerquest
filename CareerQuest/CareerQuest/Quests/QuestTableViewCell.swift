//
//  QuestTableViewCell.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 07/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

class QuestTableViewCell: UITableViewCell {
    
    @IBOutlet var category: UILabel!
    @IBOutlet var subCategory: UILabel!
    @IBOutlet var quest: UILabel!
    
    var complete: Bool = false
    
}
