//
//  File.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 07/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

class QuestDetailViewController: UIViewController {

    @IBOutlet var titleLabel: UIButton!
    @IBOutlet var categoryLabel: UIButton!
    @IBOutlet var subCategoryLabel: UIButton!
    @IBOutlet var dateStartedLabel: UIButton!
    @IBOutlet var completeQuestButton: UIButton!
    
    var quest: Quest!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        configureQuestDetails()
        buttonWrapping()
    }
    
    private func buttonWrapping() {
        categoryLabel.titleLabel?.lineBreakMode = .byWordWrapping
        subCategoryLabel.titleLabel?.lineBreakMode = .byWordWrapping
        titleLabel.titleLabel?.lineBreakMode = .byWordWrapping
    }

    private func configureQuestDetails() {
        completeQuestButton.cutCorners()
        titleLabel.setTitle(quest.title, for: .normal)
        categoryLabel.setTitle(quest.category, for: .normal)
        subCategoryLabel.setTitle(quest.subCategory, for: .normal)
        let date = Date()
        let str = String(describing: date)
        dateStartedLabel.setTitle(str, for: .normal)
    }
    
    @IBAction func completeQuestButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
