//
//  Grade.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 07/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

enum Grade {
    case analyst
    case consultant
    case seniorConsultant
    case manager
    case seniorManager
    case director
    case partner
}
