//
//  ProfileViewController.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 07/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseFirestore

class ProfileViewController: UIViewController {
    
    @IBOutlet var usernameButton: UIButton!
    @IBOutlet var gradeButton: UIButton!
    var grade: Grade = .analyst
    
    @IBOutlet var badgeCollectionView: UICollectionView!
    @IBOutlet var questTableView: UITableView!
    
    var badges: [UIImage?] = [] //[Badge] = []
    var quests: [(String, String, String, Int)] = [] //Quest] = []
    var ourQuests: [(String, String)] = []
    var stuffLoaded = false
    
    override func loadView() {
        super.loadView()
        
        
        
        let db = Firestore.firestore()
        
        
        //Data gets pulled from cloud, but tableView runs before this returns, maybe(?)
        db.collection("quests").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                //                ourQuests = querySnapshot!.documents
                for document in querySnapshot!.documents {
                    self.ourQuests.append((document.get("name") as! String, document.get("category") as! String))
                    //                    print("\(document.documentID) => \(document.data())")
                }
                self.stuffLoaded = true
                self.questTableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        usernameButton.cutCorners()
        
        checkUsername()
        
        gradeButton.setTitle("A", for: .normal)
        quests = [("Course", "UX", "Completed UX 202", 2), ("Certifications", "Business Analyst", "Scrum Master Expert Certification", 1), ("Languages", "Java", "Read Clean Code", 1), ("Languages", "JavaScript", "Created my personal website", 2), ("Languages", "Python", "Took Google Machine Learning Course", 2), ("Languages", "JavaScript", "Learned Advanced Node on LinkedIn Learning", 3), ("Languages", "Java", "Completed Advanced OOP on Pluralsight", 1), ("Platforms", "iOS", "Published my own app onto the App Store", 1), ("Platforms", "AWS", "Completed AWS certification", 3), ("Course", "Android", "Completed mobile development course on Pluralsight", 2), ("Languages", "Java", "Taught Java to new joiner while managing them", 1)]

        badges = [UIImage(named: "Completed a course in Agile 101"), UIImage(named: "Awarded a Limelight"), UIImage(named: "Completed a course in java"), UIImage(named: "Completed a course in TMT 101"), UIImage(named: "Trained in Salesforce"), UIImage(named: "Completed a course in Python"), UIImage(named: "Trained in Sprinklr"), UIImage(named: "Installed the app")]
        
        //quests = []
        badgeCollectionView.dataSource = self
        badgeCollectionView.delegate = self
        
        questTableView.dataSource = self
        questTableView.delegate = self
        
        badgeCollectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        checkForFakeBadge()
    }
    
    func checkUsername() {
        // TODO
        // core data - checks for username
        // if username exists, button is deactivated and title is username
        // saves username to variables
    }
    
    private func checkForFakeBadge() {
        if Variables.fraudulentBadge {
            badges.insert(UIImage(named: "Attended my first mashup day"), at: 0)
            Variables.fraudulentBadge = false
            badgeCollectionView.reloadData()
        }
    }

    @IBAction func gradeButtonPressed(_ sender: Any) {
        switch gradeButton.titleLabel?.text {
        case "A":
            gradeButton.setTitle("C", for: .normal)
            Variables.grade = "C"
            grade = Grade.consultant
        case "C":
            gradeButton.setTitle("SC", for: .normal)
            Variables.grade = "SC"
            grade = Grade.seniorConsultant
        case "SC":
            gradeButton.setTitle("M", for: .normal)
            Variables.grade = "M"
            grade = Grade.manager
        case "M":
            gradeButton.setTitle("SM", for: .normal)
            Variables.grade = "SM"
            grade = Grade.seniorManager
        case "SM":
            gradeButton.setTitle("D", for: .normal)
            Variables.grade = "D"
            grade = Grade.director
        case "D":
            gradeButton.setTitle("P", for: .normal)
            Variables.grade = "P"
            grade = Grade.partner
        case "P":
            gradeButton.setTitle("A", for: .normal)
            Variables.grade = "A"
            grade = Grade.analyst
        default:
            gradeButton.setTitle("A", for: .normal)
            Variables.grade = "A"
            grade = Grade.analyst
            
        }
//        saveGradeToCoreData()
    }
    
//    @IBAction func screenTapped(_ sender: Any) {
//        usernameTextField.resignFirstResponder()
//    }

    @IBAction func usernameButtonPressed(_ sender: Any) {
        let ac = UIAlertController(title: "Create Username", message: "This cannot be changed once set", preferredStyle: .alert)
        let save = UIAlertAction(title: "Save", style: .default) { (alertAction) in
            let textField = ac.textFields![0] as UITextField
            let username = textField.text
            self.usernameButton.setTitle(username, for: .normal)
            self.saveUsernameToCoreData(username)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        ac.addTextField { (textField) in
            textField.placeholder = "enter username"
        }
        ac.addAction(save)
        ac.addAction(cancel)
        present(ac, animated:true, completion: nil)
    }
    
    func saveUsernameToCoreData(_ username: String?) {
        guard let name = username else { return }
        // TODO
        // core data stuff
    }
    
    func saveGradeToCoreData() {
        //TODO
    }
    
}

extension ProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate {
        
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return badges.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "badgeCell", for: indexPath) as!   BadgeCollectionViewCell
        cell.badgeImage.image = badges[indexPath.row] //badgeIma = badges[indexPath.row]
        return cell
    }
    
//    // MARK: - View item
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if filteredMaterials.count != 0 {
//            performSegue(withIdentifier: "materialDetailSegue", sender: filteredMaterials[indexPath.row])
//        }
//        else {
//            performSegue(withIdentifier: "materialDetailSegue", sender: materials[indexPath.row])
//        }
//    }
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let materialDetailVC = segue.destination as? MaterialDetailViewController {
//            if let material = sender as? Material {
//                materialDetailVC.material = material
//                materialDetailVC.token = super.token
//            }
//        }
//    }
    
    

}

extension ProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quests.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "questCell") as! QuestTableViewCell
        cell.category.text = quests[indexPath.row].0
        cell.subCategory.text = quests[indexPath.row].1
        cell.quest.text = quests[indexPath.row].2
        switch quests[indexPath.row].3 {
        case 1:
            cell.backgroundColor = UIColor.pastelBlue()
        case 2:
            cell.backgroundColor = UIColor.pastelGreen()
        case 3:
            cell.backgroundColor = UIColor.pastelRed()
        default:
            cell.backgroundColor = UIColor.white
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let questVC = UIStoryboard(name: "Quests", bundle: nil).instantiateViewController(withIdentifier: "QuestDetailViewController") as! QuestDetailViewController
        questVC.quest = Quest(username: "jbartlett", grade: "A", title: quests[indexPath.row].2)
        questVC.quest.category = "Courses"
        questVC.quest.subCategory = "Internal"
        navigationController?.pushViewController(questVC, animated: true)
    }
    
    //TODO - delegate methods to click on quest, go to quest detail view 
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "questCell") as! QuestTableViewCell
//        cell.category.text = ourQuests[indexPath.row].0
//        cell.subCategory.text = ""
//        cell.quest.text = quests[indexPath.row].1
//        cell.backgroundColor = UIColor.white
//        return cell
//    }

}
