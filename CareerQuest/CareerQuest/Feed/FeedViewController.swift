//
//  FeedViewController.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 07/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseFirestore

class FeedViewController: UIViewController {
 
    @IBOutlet var feedQuestsTableView: UITableView!
    
    var feedQuests: [Quest] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        feedQuestsTableView.dataSource = self
        feedQuestsTableView.delegate = self
        
        let db = Firestore.firestore()
        
        db.collection("feed").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print(document.data())
                    let data = document.data()
                    
                    let username = data["user"] as! String
                    let grade = data["grade"] as! String
                    let title = data["item"] as! String
                    
                    let quest = Quest(username: username, grade: grade, title: title)
                    print(quest)
                    self.feedQuests.append(quest)
                }
                self.feedQuestsTableView.reloadData()
            }
        }
    }
    
}

extension FeedViewController: UITableViewDataSource, UITableViewDelegate {
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedQuests.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "feedCell") as! FeedTableViewCell
        cell.quest.text = feedQuests[indexPath.row].title
        cell.user.text = feedQuests[indexPath.row].username
        cell.grade.text = feedQuests[indexPath.row].grade
        
        switch indexPath.row % 3 {
        case 2:
            cell.backgroundColor = UIColor.pastelBlue()
        case 1:
            cell.backgroundColor = UIColor.pastelGreen()
        case 0:
            cell.backgroundColor = UIColor.pastelRed()
        default:
            cell.backgroundColor = UIColor.white
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let questVC = UIStoryboard(name: "Quests", bundle: nil).instantiateViewController(withIdentifier: "NewQuestViewController") as! NewQuestViewController
        questVC.prePopulated = true
        questVC.selectedCategory = "Languages"
        questVC.selectedSubCategory = "Java"
        questVC.selectedTitle = feedQuests[indexPath.row].title
        navigationController?.pushViewController(questVC, animated: true)
    }
    
}
