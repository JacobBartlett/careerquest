//
//  FeedTableViewCell.swift
//  CareerQuest
//
//  Created by Bartlett, Jacob (UK - London) on 08/06/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

class FeedTableViewCell: UITableViewCell {
    
    @IBOutlet var user: UILabel!
    @IBOutlet var grade: UILabel!
    @IBOutlet var quest: UILabel!

}
